<?php
/**
 * @file
 * ns_prod_enterprise.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ns_prod_enterprise_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-ns-prod-entrpr-footer-sec.
  $menus['menu-ns-prod-entrpr-footer-sec'] = array(
    'menu_name' => 'menu-ns-prod-entrpr-footer-sec',
    'title' => 'Secondary footer',
    'description' => 'Add links to pages like legal information here.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Add links to pages like legal information here.');
  t('Secondary footer');


  return $menus;
}
