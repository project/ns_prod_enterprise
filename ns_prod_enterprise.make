api = 2

core = 7.x

projects[adaptive_image][version] = 1.4
projects[adaptive_image][subdir] = contrib
projects[addressfield][version] = 1.0-beta3
projects[addressfield][subdir] = contrib
projects[context_admin][version] = 1.1
projects[context_admin][subdir] = contrib
projects[fences][version] = 1.0
projects[fences][subdir] = contrib
projects[fape][version] = 1.1
projects[fape][subdir] = contrib
projects[smartcrop][version] = 1.0-beta2
projects[smartcrop][subdir] = contrib
projects[media_vimeo][version] = 1.0-beta5
projects[media_vimeo][subdir] = contrib
projects[search_api][version] = 1.2
projects[search_api][subdir] = contrib
projects[search_api_db][version] = 1.0-beta3
projects[search_api_db][subdir] = contrib
projects[facetapi][version] = 1.1
projects[facetapi][subdir] = contrib
projects[views_bulk_operations][version] = 3.0-rc1
projects[views_bulk_operations][subdir] = contrib
projects[geofield][version] = 1.1
projects[geofield][subdir] = contrib
projects[geophp][version] = 1.4
projects[geophp][subdir] = contrib
projects[openlayers][version] = 2.0-beta1
projects[openlayers][subdir] = contrib
projects[geocoder][version] = 1.1
projects[geocoder][subdir] = contrib


; Semantic panels doesn't have a stable release yet.
projects[semantic_panels][type] = module
projects[semantic_panels][version] = 1.x
projects[semantic_panels][download][type] = git
projects[semantic_panels][download][revision] = 2b691f0
projects[semantic_panels][subdir] = contrib

; Content menu module doesn't have a stable release yet.
projects[content_menu][type] = module
projects[content_menu][version] = 1.x
projects[content_menu][download][type] = git
projects[content_menu][download][revision] = 155a1e3
projects[content_menu][subdir] = contrib

; Themes
projects[lucid][type] = theme
projects[lucid][version] = 1.x
projects[lucid][download][type] = git
projects[lucid][download][revision] = 2e70b36
projects[lucid][subdir] = contrib
