<?php
/**
 * @file
 * ns_prod_enterprise.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ns_prod_enterprise_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '2',
  );

  // Exported role: blogger.
  $roles['blogger'] = array(
    'name' => 'blogger',
    'weight' => '3',
  );

  // Exported role: chief editor.
  $roles['chief editor'] = array(
    'name' => 'chief editor',
    'weight' => '4',
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => '5',
  );

  // Exported role: super user.
  $roles['super user'] = array(
    'name' => 'super user',
    'weight' => '7',
  );

  // Exported role: translator.
  $roles['translator'] = array(
    'name' => 'translator',
    'weight' => '11',
  );

  // Exported role: writer.
  $roles['writer'] = array(
    'name' => 'writer',
    'weight' => '10',
  );

  return $roles;
}
