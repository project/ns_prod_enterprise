<?php
/**
 * @file
 * ns_prod_enterprise_client.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ns_prod_enterprise_client_taxonomy_default_vocabularies() {
  return array(
    'ns_prod_enterprise_case_study' => array(
      'name' => 'Case study',
      'machine_name' => 'ns_prod_enterprise_case_study',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
