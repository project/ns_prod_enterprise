<?php
/**
 * @file
 * ns_prod_enterprise_office.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ns_prod_enterprise_office_taxonomy_default_vocabularies() {
  return array(
    'ns_prod_enterprise_office' => array(
      'name' => 'Office',
      'machine_name' => 'ns_prod_enterprise_office',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
