<?php
/**
 * @file
 * ns_prod_enterprise_search.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ns_prod_enterprise_search_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'ns_prod_enterprise_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_ns_prod_enterprise_node';
  $view->human_name = 'Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Node: Entity label */
  $handler->display->display_options['fields']['label']['id'] = 'label';
  $handler->display->display_options['fields']['label']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['label']['field'] = 'label';
  $handler->display->display_options['fields']['label']['label'] = '';
  $handler->display->display_options['fields']['label']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['label']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['label']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['label']['alter']['external'] = 0;
  $handler->display->display_options['fields']['label']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['label']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['label']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['label']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['label']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['label']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['label']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['label']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['label']['alter']['html'] = 0;
  $handler->display->display_options['fields']['label']['element_type'] = 'h2';
  $handler->display->display_options['fields']['label']['element_class'] = 'title';
  $handler->display->display_options['fields']['label']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['label']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['label']['hide_empty'] = 0;
  $handler->display->display_options['fields']['label']['empty_zero'] = 0;
  $handler->display->display_options['fields']['label']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['label']['link_to_entity'] = 1;
  /* Field: Indexed Node: Media */
  $handler->display->display_options['fields']['field_ns_blog_post_media']['id'] = 'field_ns_blog_post_media';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['field'] = 'field_ns_blog_post_media';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['label'] = '';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['settings'] = array(
    'file_view_mode' => 'media_large',
  );
  $handler->display->display_options['fields']['field_ns_blog_post_media']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_ns_blog_post_media']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_media']['field_api_classes'] = 0;
  /* Field: Indexed Node: Media */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['id'] = 'field_ns_prod_entrpr_media';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['field'] = 'field_ns_prod_entrpr_media';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['settings'] = array(
    'file_view_mode' => 'ns_prod_entrprise_large',
  );
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_media']['field_api_classes'] = 0;
  /* Field: Indexed Node: Media */
  $handler->display->display_options['fields']['field_ns_page_media']['id'] = 'field_ns_page_media';
  $handler->display->display_options['fields']['field_ns_page_media']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['fields']['field_ns_page_media']['field'] = 'field_ns_page_media';
  $handler->display->display_options['fields']['field_ns_page_media']['label'] = '';
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_page_media']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ns_page_media']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_page_media']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_ns_page_media']['type'] = 'file_rendered';
  $handler->display->display_options['fields']['field_ns_page_media']['settings'] = array(
    'file_view_mode' => 'ns_prod_entrprise_large',
  );
  $handler->display->display_options['fields']['field_ns_page_media']['delta_limit'] = '1';
  $handler->display->display_options['fields']['field_ns_page_media']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_ns_page_media']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['delta_first_last'] = 0;
  $handler->display->display_options['fields']['field_ns_page_media']['field_api_classes'] = 0;
  /* Field: Indexed Node: Lead */
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['id'] = 'field_ns_blog_post_lead';
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['field'] = 'field_ns_blog_post_lead';
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['label'] = '';
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_blog_post_lead']['field_api_classes'] = 0;
  /* Field: Indexed Node: Lead */
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['id'] = 'field_ns_prod_entrpr_lead';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['field'] = 'field_ns_prod_entrpr_lead';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['label'] = '';
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_prod_entrpr_lead']['field_api_classes'] = 0;
  /* Field: Indexed Node: Lead */
  $handler->display->display_options['fields']['field_ns_page_lead']['id'] = 'field_ns_page_lead';
  $handler->display->display_options['fields']['field_ns_page_lead']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['fields']['field_ns_page_lead']['field'] = 'field_ns_page_lead';
  $handler->display->display_options['fields']['field_ns_page_lead']['label'] = '';
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_ns_page_lead']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_ns_page_lead']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_ns_page_lead']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_ns_page_lead']['field_api_classes'] = 0;
  /* Sort criterion: Search: Relevance */
  $handler->display->display_options['sorts']['search_api_relevance']['id'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['sorts']['search_api_relevance']['field'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['order'] = 'DESC';
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_ns_prod_enterprise_node';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';

  /* Display: Content pane */
  $handler = $view->new_display('panel_pane', 'Content pane', 'panel_pane_1');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['inherit_panels_path'] = '1';
  $translatables['ns_prod_enterprise_search'] = array(
    t('Master'),
    t('more'),
    t('Search'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('Content pane'),
    t('View panes'),
  );
  $export['ns_prod_enterprise_search'] = $view;

  return $export;
}
