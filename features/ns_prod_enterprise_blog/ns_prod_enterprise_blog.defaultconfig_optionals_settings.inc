<?php
/**
 * @file
 * ns_prod_enterprise_blog.defaultconfig_optionals_settings.inc
 */

/**
 * Implements hook_defaultconfig_optionals_settings().
 */
function ns_prod_enterprise_blog_defaultconfig_optionals_settings() {
  $export = array();

  $preset = new stdClass();
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->name = 'field_default_fields:node-ns_blog_post-field_ns_blog_post_lead';
  $preset->status = 1;
  $export['field_default_fields:node-ns_blog_post-field_ns_blog_post_lead'] = $preset;

  return $export;
}
