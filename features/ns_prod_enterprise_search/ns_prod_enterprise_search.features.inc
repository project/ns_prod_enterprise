<?php
/**
 * @file
 * ns_prod_enterprise_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_prod_enterprise_search_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ns_prod_enterprise_search_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function ns_prod_enterprise_search_default_search_api_index() {
  $items = array();
  $items['ns_prod_enterprise_node'] = entity_import('search_api_index', '{
    "name" : "ns_prod_enterprise_node",
    "machine_name" : "ns_prod_enterprise_node",
    "description" : null,
    "server" : "ns_prod_enterprise_db",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "type" : { "type" : "string" },
        "author" : { "type" : "integer", "entity_type" : "user" },
        "title_field" : { "type" : "text" },
        "field_ns_page_lead" : { "type" : "text" },
        "field_ns_blog_post_lead" : { "type" : "text" },
        "field_ns_prod_entrpr_lead" : { "type" : "text" },
        "field_ns_prod_entrpr_case_study" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "search_api_language" : { "type" : "string" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 1,
          "weight" : "-10",
          "settings" : {
            "default" : "1",
            "bundles" : { "ns_fact" : "ns_fact", "ns_form" : "ns_form", "ns_promo" : "ns_promo" }
          }
        },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : { "status" : 0, "weight" : "0", "settings" : [] }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : { "title_field" : true } }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : { "title_field" : true },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : { "title_field" : true },
            "spaces" : "[^\\\\p{L}\\\\p{N}^\\u0027]",
            "ignorable" : "[-]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : { "title_field" : true },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function ns_prod_enterprise_search_default_search_api_server() {
  $items = array();
  $items['ns_prod_enterprise_db'] = entity_import('search_api_server', '{
    "name" : "Default NodeStream enterprise server",
    "machine_name" : "ns_prod_enterprise_db",
    "description" : "",
    "class" : "search_api_db_service",
    "options" : {
      "database" : "default:default",
      "min_chars" : "1",
      "indexes" : { "ns_prod_enterprise_node" : {
          "type" : {
            "table" : "search_api_db_ns_prod_enterprise_node_type",
            "type" : "string",
            "boost" : "1.0"
          },
          "author" : {
            "table" : "search_api_db_ns_prod_enterprise_node_author",
            "type" : "integer",
            "boost" : "1.0"
          },
          "title_field" : {
            "table" : "search_api_db_ns_prod_enterprise_node_title_field",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_ns_page_lead" : {
            "table" : "search_api_db_ns_prod_enterprise_node_field_ns_page_lead",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_ns_prod_entrpr_case_study" : {
            "table" : "search_api_db_ns_prod_enterprise_node_field_ns_prod_entrpr_case",
            "type" : "list\\u003Cinteger\\u003E",
            "boost" : "1.0"
          },
          "search_api_language" : {
            "table" : "search_api_db_ns_prod_enterprise_node_search_api_language",
            "type" : "string",
            "boost" : "1.0"
          },
          "field_ns_blog_post_lead" : {
            "table" : "search_api_db_ns_prod_enterprise_node_field_ns_blog_post_lead",
            "type" : "text",
            "boost" : "1.0"
          },
          "field_ns_prod_entrpr_lead" : {
            "table" : "search_api_db_ns_prod_enterprise_node_field_ns_prod_entrpr_lead",
            "type" : "text",
            "boost" : "1.0"
          }
        }
      }
    },
    "enabled" : "1"
  }');
  return $items;
}
